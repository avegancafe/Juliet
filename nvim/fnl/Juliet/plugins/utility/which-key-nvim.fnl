(import-macros {: pack} :Juliet.macros)

(pack :folke/which-key.nvim {:config true :opts {:window {:border :rounded}}})
