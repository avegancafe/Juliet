(import-macros {: pack : key} :Juliet.macros)

(pack :sbdchd/neoformat {:keys [(key :<leader>bf ":Neoformat<cr>"
                                     "Format buffer")]})
